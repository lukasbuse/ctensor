""" MIT License

Copyright (c) 2020 Lukas Buse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import torch

class CTensor(torch.Tensor):
    @staticmethod
    def __new__(cls, x, *args, **kwargs):
        return super().__new__(cls, x, *args, **kwargs)
    
    def __init__(self, x):
        super().__init__()
            
        self.__floordiv__ = None
        self.__div__ = None
        self.__ifloordiv__ = None
        self.__idiv__ = None
        self.__itruediv__ = None
        self.__imul__ = None
        self.__iadd__ = None
        self.__isub__ = None
        
    def __neg__(self):
        return self*(-1)
    
    def __radd__(self,y):
        return self.__add__(y)
    
    def __add__(self,y):
        item = super().__add__(y)
        item.__class__ = type(self)
        return item
    
    def __sub__(self,y):
        item = super().__sub__(y)
        item.__class__ = type(self)
        return item
    
    def __rmul__(self,y):
        return self.__mul__(y)
    
    def __mul__(self,y):
        if isinstance(y, type(self)) and self.shape[1] == y.shape[1] and self.shape[1] == 2:
            return type(self).complex_mul(self,y)
        else:
            item = super().__mul__(y)
            item.__class__ = type(self)
            return item
    
    def __truediv__(self,y):
        if isinstance(y, type(self)) and self.shape[1] == y.shape[1] and self.shape[1] == 2:
            return type(self).complex_div(self,y)
        else:
            item = super().__truediv__(y)
            item.__class__ = type(self)
            return item
        
    def __matmul__(self, y):
        if isinstance(y, type(self)):
            return type(self).complex_matmul(self,y)
        else:
            raise
    
    def __abs__(self):
        return torch.sqrt((self*self.conj)[:,0:1])
        
    def __getitem__(self,key):
        item = super().__getitem__(key)
        item.__class__ = type(self)
        return item
    
    def to(self, *args, **kwargs):
        item = super().to(*args, **kwargs)
        item.__class__ = type(self)
        return item
    
    def clone(self, *args, **kwargs):
        item = super().clone(*args, **kwargs)
        item.__class__ = type(self)
        return item
    
    def view(self, *args, **kwargs):
        item = super().view(*args, **kwargs)
        item.__class__ = type(self)
        return item
    
    def contiguous(self, *args, **kwargs):
        item = super().contiguous(*args, **kwargs)
        item.__class__ = type(self)
        return item
    
    @classmethod
    def cat(cls, *args, **kwargs):
        item = torch.cat(*args, **kwargs)
        item.__class__ = cls
        return item
    
    def mean(self, *args, **kwargs):
        item = super().mean(*args, **kwargs)
        item.__class__ = type(self)
        return item
    
    def sum(self, *args, **kwargs):
        item = super().sum(*args, **kwargs)
        item.__class__ = type(self)
        return item
    
    def abs(self):
        return self.__abs__()
    
    def angle(self):
        return torch.atan2(self.real,self.imag)
    
    @property
    def real(self):
        r = self[CTensor.__selection_slice(self,0)]
        im = torch.zeros(r.shape,dtype=r.dtype).to(r.device)
        ctensor = torch.cat((r,im),dim=1)
        ctensor.__class__ = type(self)
        return ctensor
    
    @property
    def imag(self):
        r = self[CTensor.__selection_slice(self,1)]
        im = torch.zeros(r.shape,dtype=r.dtype).to(r.device)
        ctensor = torch.cat((r,im),dim=1)
        ctensor.__class__ = type(self)
        return ctensor
    
    @property
    def conj(self):
        v = [2 if i==1 else 1 for i in range(len(self.shape))]
        return self*torch.tensor([1,-1]).view(v).to(self.device,dtype=self.dtype)

    @property
    def T(self):
        return type(self).transpose(self)
    
    @classmethod
    def transpose(cls, x, dim0=-2, dim1=-1):
        item = torch.transpose(x, dim0=dim0, dim1=dim1)
        item.__class__ = cls
        return item    

    @property
    def H(self):
        return type(self).hermitian(self)
    
    @classmethod
    def hermitian(cls, x, dim0=-2, dim1=-1):
        return cls.transpose(x.conj)
    
    @property
    def raw(self):
        return super().__getitem__(slice(None,None,None))
    
        
    @classmethod
    def complex_mul(cls, x, y):
        item = torch.cat(
            (x[:,0:1].raw*y[:,0:1].raw - x[:,1:2].raw*y[:,1:2].raw,
             x[:,0:1].raw*y[:,1:2].raw + y[:,0:1].raw*x[:,1:2].raw),
            dim=1)
        item.__class__ = cls
        return item
    
    @classmethod
    def complex_div(cls, x, y, epsilon=0.00001):
        return cls.complex_mul(x,y.conj)/(y.real[:,0:1]**2 + y.imag[:,1:2]**2 + epsilon)

    
    @classmethod
    def __selection_slice(cls, ct, re_im):
        return [(slice(int(re_im),int(re_im)+1) if i == 1 else slice(None)) for i in range(len(ct.shape))]
    
    @classmethod
    def t2r(cls,x):
        im = torch.zeros(x.shape,dtype=x.dtype)
        v = [1 if i < 2 else x.shape[i-2] for i in range(len(x.shape)+2)]
        ctensor = torch.cat((x.view(v),im.view(v)),dim=1)
        return cls(ctensor)
    
    @classmethod
    def t2i(cls,x):
        re = torch.zeros(x.shape,dtype=x.dtype)
        v = [1 if i < 2 else x.shape[i-2] for i in range(len(x.shape)+2)]
        ctensor = torch.cat((re.view(v),x.view(v)),dim=1)
        return cls(ctensor)
    
    @classmethod
    def j(cls,x):
        re = torch.zeros(x[:,0:1].shape,dtype=x.dtype).to(x.device)
        ctensor = torch.cat((re,x[:,0:1]),dim=1)
        ctensor.__class__ = cls
        return ctensor
